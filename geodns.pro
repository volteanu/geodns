TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    host.cc \
    staticarr.cc \
    main.cc \
    geodnscontext.cc


unix:!macx: LIBS += -lGeoIP

HEADERS += \
    host.hh \
    staticarr.hh \
    geodnscontext.hh
