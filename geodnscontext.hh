#ifndef GEODNS_GEODNSCONTEXT_HH
#define GEODNS_GEODNSCONTEXT_HH

#include <vector>
#include "host.hh"
#include "staticarr.hh"

namespace GeoDNS
{

class GeoDNSContext
{
	GeoIP *geo;
	const char *fqdn;
	std::vector<Host> servers;
	StaticARR arr;

public:	
	GeoDNSContext(const char *fqdn, const char *serverListName, const char *db);

	~GeoDNSContext();
	
	uint32_t nearestIP(uint32_t ip);

	uint32_t defaultIP();
	
	void prime(uint16_t transaction, uint32_t client);
	
	size_t getResponseSize() const
	{
		return arr.getSize();
	}

	void *getResponse() const
	{
		return arr.getResponse();
	}
};

}

#endif // GEODNS_GEODNSCONTEXT_HH
