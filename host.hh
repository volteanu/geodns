#ifndef GEODNS_HOST_HH
#define GEODNS_HOST_HH

#include <GeoIPCity.h>

namespace GeoDNS
{

struct Host
{
	uint32_t ip;
	double lng;
	double lat;

	Host(uint32_t ip, double lng, double lat)
		: ip(ip), lng(lng), lat(lat) {}

	Host(const char *ip, GeoIP *geo);

	Host(uint32_t ip, GeoIP *geo);

	double distanceTo(Host *other);
};

}

#endif // GEODNS_GEODNS_HOST_HH
