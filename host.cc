#include <math.h>
#include <stdexcept>
#include <string>
#include <arpa/inet.h>
#include "host.hh"

using namespace std;

namespace GeoDNS
{

Host::Host(const char *ip, GeoIP *geo)
{
	in_addr in;
	
	if (!inet_aton(ip, &in))
		throw runtime_error(string("Bad IP: ") + ip);
	
	*this = Host(in.s_addr, geo);
}

Host::Host(uint32_t ip, GeoIP *geo)
	: ip(ip)
{
	GeoIPRecord *record = GeoIP_record_by_ipnum(geo, ntohl(ip));

	if (!record)
	{
		in_addr in;
		in.s_addr = this->ip;
		throw runtime_error(string("No GeoIP record: ") + inet_ntoa(in));
	}

	lng = (double)record->longitude * M_PI / 180;
	lat = (double)record->latitude  * M_PI / 180;

	GeoIPRecord_delete(record);
}

double Host::distanceTo(Host *other)
{
	return acos(sin(lat) * sin(other->lat) + cos(lat) * cos(other->lat) * cos(other->lng - lng));
}

}
