#ifndef GEODNS_STATICARR_HH
#define GEODNS_STATICARR_HH

#include <arpa/inet.h>
#include <string>

namespace GeoDNS
{

/* ARR, matey! */
class StaticARR
{
	struct Header
	{
		uint16_t transaction;
		uint16_t flags;
		uint16_t questions;
		uint16_t answerrrs;
		uint16_t authorityrrs;
		uint16_t additionalrrs;

		Header()
			: flags(htons(0x8180)), questions(htons(1)), answerrrs(htons(1)), authorityrrs(0), additionalrrs(0) {}
	} __attribute__((__packed__));

	struct QueryFooter
	{
		uint16_t type;
		uint16_t cls;

		QueryFooter()
			: type(htons(1)), cls(htons(1)) {}
	} __attribute__((__packed__));

	struct RRFooter
	{
		static const uint32_t TTL = 60;

		uint16_t type;
		uint16_t cls;
		uint32_t ttl;
		uint16_t datalen;
		uint32_t ip;

		RRFooter()
			: type(htons(1)), cls(htons(1)), ttl(htonl(TTL)), datalen(htons(4)) {}
	} __attribute__((__packed__));

	char *buffer;
	uint16_t *transaction;
	uint32_t *server;
	size_t size;

public:
	StaticARR(const char *matey);

	~StaticARR();

	void setFields(uint16_t transaction, uint32_t server) const;

	size_t getSize() const;

	void *getResponse() const;
	
private:
	static std::string *toDnsString(const std::string &str);
};

}

#endif // GEODNS_STATICARR_HH
