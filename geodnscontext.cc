#include <fstream>
#include <math.h>
#include <iostream>
#include <boost/foreach.hpp>
#include "geodnscontext.hh"

using namespace std;

namespace GeoDNS
{

GeoDNSContext::GeoDNSContext(const char *fqdn, const char *serverListName, const char *db)
	: fqdn(fqdn), arr(fqdn)
{
	ifstream serverList(serverListName, ios_base::in);

	geo = GeoIP_open(db, GEOIP_MEMORY_CACHE);
	if (!geo)
		throw runtime_error("Bad GeoIP DB");

	while (!serverList.eof())
	{
		string serverName;

		getline(serverList, serverName);
		if (!serverName.length())
			continue;

		try
		{
			servers.push_back(Host(serverName.c_str(), geo));
		}
		catch (const exception &ex)
		{
			cerr << ex.what() << endl;
		}
	}

	if (!servers.size())
		throw runtime_error("No servers");
}

GeoDNSContext::~GeoDNSContext()
{
	GeoIP_delete(geo);
}

uint32_t GeoDNSContext::nearestIP(uint32_t ip)
{
	double maxDistance = 2 * M_PI + 1; /* greater than any possible distance */
	Host source(ip, geo);
	uint32_t ret = 0;
	
	BOOST_FOREACH(Host server, servers)
	{
		double distance = server.distanceTo(&source);
		
		if (distance < maxDistance)
		{
			maxDistance = distance;
			ret = server.ip;
		}
	}
	
	return ret;
}

uint32_t GeoDNSContext::defaultIP()
{
	return servers[0].ip;
}

void GeoDNSContext::prime(uint16_t transaction, uint32_t client)
{
	try
	{
		arr.setFields(transaction, nearestIP(client));
	}
	catch (const exception &ex)
	{
		cerr << ex.what() << "; sending default IP" << endl;
		arr.setFields(transaction, defaultIP());
	}
}

}
