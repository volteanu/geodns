#include <iostream>
#include <GeoIPCity.h>
#include <vector>
#include <math.h>
#include <exception>
#include <string>
#include <errno.h>
#include "host.hh"
#include "geodnscontext.hh"

using namespace std;
using namespace GeoDNS;

struct DNSHeaderStub
{
	uint16_t transaction;
};

const int DNS_PORT = 53;

void fatal(const string &str)
{
	cerr << str << endl;
	exit(EXIT_FAILURE);
}

void errnoFatal(const string &str)
{
	fatal(str + ": " + strerror(errno));
}

void usage()
{
	fatal("usage: geodns <fqdn> <server list> <database>");
}

void serverLoop(GeoDNSContext *ctx, int sock)
{
	char buf[1500]; // yes, I'm lazy
	DNSHeaderStub *header = reinterpret_cast<DNSHeaderStub *>(buf);
	ssize_t bytes;
	sockaddr_in remoteAddr;
	socklen_t remoteAddrLen = sizeof(remoteAddr);
	
	while (true)
	{		
		bytes = recvfrom(sock, buf, 1500, 0, reinterpret_cast<sockaddr *>(&remoteAddr), &remoteAddrLen);
		if (bytes < 0)
		{
			if (errno == EAGAIN)
				continue;
			errnoFatal("recvfrom");
		}
		
		/* spurious message */
		if (bytes < (int)sizeof(DNSHeaderStub))
			continue;
		
		/* assuming we received an A query */
		
		try
		{
			ctx->prime(header->transaction, remoteAddr.sin_addr.s_addr);
		}
		catch (const exception &ex)
		{
			cerr << ex.what() << endl;
			continue;
		}
		
resend:
		bytes = sendto(sock, ctx->getResponse(), ctx->getResponseSize(), 0, reinterpret_cast<sockaddr *>(&remoteAddr), remoteAddrLen);
		if (bytes < 0)
		{
			if (errno == EAGAIN)
				goto resend;
			errnoFatal("sendto");
		}
	}
}

int main(int argc, char **argv)
{
	if (argc != 4)
		usage();
	
	try
	{
		GeoDNSContext ctx(argv[1], argv[2], argv[3]);
		int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		sockaddr_in localAddr;
		int err;
		
		if (sock < 0)
			fatal(string("socket: ") + strerror(errno));
		
		memset(&localAddr, 0, sizeof(localAddr));
		localAddr.sin_family = AF_INET;
		localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
		localAddr.sin_port = htons(DNS_PORT);
		
		err = bind(sock, reinterpret_cast<sockaddr *>(&localAddr), sizeof(localAddr));
		if (err < 0)
			errnoFatal("socket");
		
		serverLoop(&ctx, sock);
	}
	catch (const exception &ex)
	{
		fatal(ex.what());
	}

	return EXIT_SUCCESS;
}

