#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <string.h>
#include "staticarr.hh"

using namespace std;
using namespace boost;

namespace GeoDNS
{

/* due to laziness, we won't check if the string is a valid URL */
string *StaticARR::toDnsString(const string &str)
{
	string *ret = new string();
	char_separator<char> separator(".");
	tokenizer<char_separator<char> > tok(str, separator);
	
	BOOST_FOREACH(string token, tok)
	{
		if (token.length() > 255)
		{
			delete ret;
			throw runtime_error("Bad URL");
		}
		ret->append(1, (char)token.length());
		ret->append(token);
	}
	return ret;
}

StaticARR::StaticARR(const char *matey)
{
	char *crt;
	string *dnsString = toDnsString(string(matey));
	
	size = sizeof(Header) + /* header */
		dnsString->length() + 1 + sizeof(QueryFooter) + /* query */
		dnsString->length() + 1 + sizeof(RRFooter); /* RR */

	buffer = new char[size];
	crt = buffer;
	
	/* Header */
	{
		Header *header = reinterpret_cast<Header *>(crt);
		
		*header = Header();
		crt += sizeof(Header);
		
		transaction = &header->transaction;
	}
	
	/* Query */
	{
		QueryFooter *queryFooter;
		
		memcpy(crt, dnsString->c_str(), dnsString->length() + 1);
		crt += dnsString->length() + 1;
		
		queryFooter = reinterpret_cast<QueryFooter *>(crt);
		*queryFooter = QueryFooter();
		crt +=sizeof(QueryFooter);
	}
	
	/* RR */
	{
		RRFooter *rrFooter;
		memcpy(crt, dnsString->c_str(), dnsString->length() + 1);
		crt += dnsString->length() + 1;
		
		rrFooter = reinterpret_cast<RRFooter *>(crt);
		*rrFooter = RRFooter();
		crt +=sizeof(RRFooter);
		
		server = &rrFooter->ip;
	}
	
	delete dnsString;
}

StaticARR::~StaticARR()
{
	delete buffer;
}

void StaticARR::setFields(uint16_t transaction, uint32_t server) const
{
	*this->transaction = transaction;
	*this->server = server;
}

size_t StaticARR::getSize() const
{
	return size;
}

void *StaticARR::getResponse() const
{
	return buffer;
}

}
